(function (ng) {
    glbApp.controller("contactCtrl", ['$rootScope', '$scope', '$location', '$filter', 'contactService', '$mdDialog', 'assetURL', 'growl', '$cookies', '$confirm', function ($rootScope, $scope, $location, $filter, contactService, $mdDialog, assetURL, growl, $cookies, $confirm) {
            var isLoggedIn = $cookies.get('loggedIn');
            if (typeof isLoggedIn == "undefined") {
                $location.path('/');
            }
            var ctrlObj = this;
            $rootScope.contactType = {"1": "Mobile", "2": 'Home', "3": "Fax", "4": "Office"};
            $rootScope.selectedMenu = 'contacts';
            contactService.getcontacts(function (res) {
                res = res.Contacts;
                res = ctrlObj.reArrangePattern(res);
                $scope.collection = [];
                angular.forEach(res, function (val, key) {
                    var contactObj = {};
                    contactObj.id = val.id;
                    contactObj.firstName = val.firstName;
                    contactObj.lastName = val.lastName;
                    contactObj.emailId = val.emailId;
                    contactObj.number = val.contacts[0]['cnum'];
                    contactObj.contactInfo = val;
                    $scope.collection.push(contactObj);
                });
                $scope.itemsByPage = 15;
                $scope.displayed = [].concat($scope.collection);
            });
            ctrlObj.reArrangePattern = function (res) {
                var rtnArr = [];
                angular.forEach(res, function (val, key) {
                    var contactObj = {};
                    contactObj.id = val.id;
                    contactObj.firstName = val.firstName;
                    contactObj.lastName = val.lastName;
                    contactObj.middleName = val.middleName;
                    contactObj.emailId = val.emailId;
                    contactObj.contacts = ctrlObj.contactsMap(val.phoneNoList);
                    rtnArr.push(contactObj);
                });
                return rtnArr;
            }
            ctrlObj.contactsMap = function (contacts) {
                var contactInfo = [];
                angular.forEach(contacts, function (val, key) {
                    for (var i = 0; i < val.length; i++) {
                        var subContact = {};
                        subContact['ctype'] = key;
                        subContact['cnum'] = val[i];
                        contactInfo.push(subContact);
                    }
                });
                return contactInfo;
            }
            $scope.showContactInfo = function (event, row) {
                $scope.contactDialogInfo = row;
                $scope.assetUrl = assetURL.url;
                $mdDialog.show({
                    clickOutsideToClose: true,
                    scope: $scope,
                    preserveScope: true,
                    templateUrl: 'contactInfo.tmpl.html',
                    controller: contactDialogController,
                    openFrom: {top: 1500},
                    closeTo: {left: 1500}
                });
            };
            $scope.editContact = function ($event, row) {
                $event.stopPropagation();
                $rootScope.selectedContact = row;
                $location.path('/edit-contact');
            }
            $scope.deleteContact = function ($event, row) {
                $event.stopPropagation();
                $confirm({text: 'Are you sure you want to delete?'}).then(function () {
                    var id = row.id;
                    contactService.removeContact(id, function (res) {
                        if (res.Status == 1) {
                            growl.success('Contact has been deleted successfully..', {title: 'Success!'});
                            var index = $scope.collection.indexOf(row);
                            if (index !== -1) {
                                $scope.collection.splice(index, 1);
                            }
                        } else {
                            growl.success("OOp's something went wrong while deleting a Contact..", {title: 'Danger!'});
                        }
                    });
                });
            }
            function contactDialogController($scope, $mdDialog) {
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
            }
        }]);
    glbApp.controller("addContactCtrl", ['$rootScope', '$scope', '$location', 'contactService', 'growl', '$cookies', function ($rootScope, $scope, $location, contactService, growl, $cookies) {
            var isLoggedIn = $cookies.get('loggedIn');
            if (typeof isLoggedIn == "undefined") {
                $location.path('/');
            }
            $rootScope.selectedMenu = 'contacts';
            var ctrlObj = this;
            if (typeof $rootScope.selectedContact != "undefined") {
                ctrlObj.choices = $rootScope.selectedContact.contactInfo.contacts;
                ctrlObj.cntc = $rootScope.selectedContact;
            } else {
                ctrlObj.choices = [{ctype: 'Mobile'}];
            }
            ctrlObj.addNewChoice = function () {
                var newItemNo = ctrlObj.choices.length + 1;
                if (ctrlObj.choices.length < 5) {
                    ctrlObj.choices.push({ctype: 'Mobile'});
                }
            };
            ctrlObj.removeChoice = function (removeItem) {
                var lastItem = ctrlObj.choices.length - 1;
                ctrlObj.choices.splice(removeItem, 1);
            };
            ctrlObj.saveContact = function (flds) {
                flds.contacts = ctrlObj.choices;
                var saveContact = flds;
                saveContact.phoneNoList = ctrlObj.arrangeSaveContacts(flds.contacts);
                delete saveContact.contacts;
                if (typeof saveContact.$$hashKey != "undefined") {
                    delete saveContact.$$hashKey;
                }
                if (typeof saveContact.contactInfo != "undefined") {
                    delete saveContact.contactInfo;
                }
                if (typeof saveContact.number != "undefined") {
                    delete saveContact.number;
                }
                contactService.putcontacts(saveContact, function (res) {
                    var message = (typeof saveContact.id != "undefined") ? 'updated' : 'created';
                    if (res.Status == 1) {
                        delete $rootScope.selectedContact;
                        growl.success('Contact has been ' + message + ' successfully.', {title: 'Success!'});
                        $location.path('/contacts');
                    } else {
                        growl.success("Oop's something went wrong while creating Contact!!!.", {title: 'Danger!'});
                    }
                });
            };
            ctrlObj.arrangeSaveContacts = function (contacts) {
                var phoneNoList = {};
                angular.forEach(contacts, function (val, key) {
                    if (typeof phoneNoList[val.ctype] == "undefined") {
                        phoneNoList[val.ctype] = [];
                    }
                    var num = val.cnum;
                    num = num.toString();
                    phoneNoList[val.ctype].push(num);
                });
                return phoneNoList;
            }
            ctrlObj.back = function () {
                $location.path('/contacts');
            };
            ctrlObj.cancel = function (flds) {
                ctrlObj.cntc = {};
                ctrlObj.choices = [{ctype: 'Mobile'}];
            };
        }]);
})(angular);