(function () {
    glbApp.controller("dashboardCtrl", ['$rootScope', '$scope', '$location', '$cookies', function ($rootScope, $scope, $location, $cookies) {
            var isLoggedIn = $cookies.get('loggedIn');
            if (typeof isLoggedIn == "undefined") {
                $location.path('/');
            }
            var ctrlObj = this;
            $rootScope.selectedMenu = 'dashboard';
        }]);
})();