(function () {
    glbApp.controller("homeCtrl", ['$rootScope', '$scope', '$location', '$cookies', function ($rootScope, $scope, $location, $cookies) {
            var isLoggedIn = $cookies.get('loggedIn');
            if (typeof isLoggedIn != "undefined" && isLoggedIn == 1) {
                $location.path('/dashboard');
            }
            if (typeof isLoggedIn != "undefined" && isLoggedIn == 2) {
                $location.path('/contacts');
            }
            var ctrlObj = this;
            ctrlObj.login_data = {};
            ctrlObj.checkLogin = function (loginInfo) {
                ctrlObj.loginError = (loginInfo.mobile_number == '1234567891') ? 1 : ((loginInfo.mobile_number == '1234567890') ? 2 : 0);
                if (ctrlObj.loginError != 0) {
                    $cookies.put('loggedIn', ctrlObj.loginError);
                    var redirect = (ctrlObj.loginError == 2) ? 'contacts' : 'dashboard';
                    $location.path('/' + redirect);
                }
            }
        }]);
    glbApp.controller("commonCtrl", ['$rootScope', '$scope', '$cookies', '$interval', 'updateRecursive', 'contactService', function ($rootScope, $scope, $cookies, $interval, updateRecursive, contactService) {
            if (typeof $rootScope.isIntervalStart == "undefined")
                $rootScope.isIntervalStart = 0;
            function contactInfo() {
                contactService.getLatestContacts(function (res) {
                    res = preparePropogate(res);
                    angular.forEach(res, function (val, key) {
                        switch (val.type) {
                            case 1:
                                if (val.count > 0) {
                                    $rootScope.addStatus = 1;
                                    $rootScope.addVal = val.count;
                                    $rootScope.addDetails = val.details;
                                }
                                break;
                            case 2:
                                if (val.count > 0) {
                                    $rootScope.updateStatus = 1;
                                    $rootScope.updateVal = val.count;
                                    $rootScope.updateDetails = val.details;
                                }
                                break;
                            case 3:
                                if (val.count > 0) {
                                    $rootScope.deleteStatus = 1;
                                    $rootScope.deleteVal = val.count;
                                    $rootScope.deleteDetails = val.details;
                                }
                                break;
                        }
                    });
                });
            }

            function preparePropogate(res) {
                var rtnArr = [];
                var typeObj = {"insert": 1, "update": 2, "delete": 3};
                if (res !== null) {
                    angular.forEach(res, function (val, key) {
                        var obj = {};
                        obj.type = typeObj[key];
                        obj.count = val.Count;
                        var detailArr = [];
                        if (obj.count > 0) {
                            angular.forEach(val.Messages, function (msgVal, msgKey) {
                                var detailObj = {};
                                detailObj.name = msgVal.firstName + msgVal.lastName;
                                detailObj.number = "";
                                angular.forEach(msgVal.phoneNoList, function (numVal, numKey) {
                                    if (typeof numVal[0] != "undefined" && detailObj.number == "") {
                                        detailObj.number = numVal[0];
                                    }
                                });
                                detailArr.push(detailObj);
                            });
                        }
                        obj.details = detailArr;
                        rtnArr.push(obj);
                    });
                }
                return rtnArr;
            }
            var isLoggedIn = $cookies.get('loggedIn');
            if (typeof isLoggedIn != "undefined") {
                $rootScope.userType = isLoggedIn;
                if ($rootScope.userType == 1 && $rootScope.isIntervalStart == 0) {
                    $rootScope.isIntervalStart = 1;
                    $interval(contactInfo, updateRecursive.duration);
                }
            }
        }]);
    glbApp.controller("logoutCtrl", ['$location', '$cookies', function ($location, $cookies) {
            $cookies.remove('loggedIn');
            $location.path('/');
        }]);
})();