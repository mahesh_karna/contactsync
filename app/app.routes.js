'use strict';
var glbApp;
(function () {
    glbApp.config(['$routeProvider', '$locationProvider', '$httpProvider', 'growlProvider', function ($routeProvider, $locationProvider, $httpProvider, growlProvider) {
            growlProvider.globalTimeToLive({success: 5000, error: 5000, warning: 3000, info: 4000});
            growlProvider.globalReversedOrder(true);
            growlProvider.onlyUniqueMessages(false);
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
            $routeProvider.
                    when('/contacts', {
                        templateUrl: 'app/components/contacts/contacts.html', controller: 'commonCtrl'
                    }).
                    when('/add-contact', {
                        templateUrl: 'app/components/contacts/addcontact.html', controller: 'commonCtrl'
                    }).
                    when('/edit-contact', {
                        templateUrl: 'app/components/contacts/addcontact.html', controller: 'commonCtrl'
                    }).
                    when('/dashboard', {
                        templateUrl: 'app/components/dashboard/dashboard.html', controller: 'commonCtrl'
                    }).
                    when('/logout', {
                        templateUrl: 'app/components/home/logout.html'
                    }).
                    when('/', {
                        templateUrl: 'app/components/home/home.html'
                    }).
                    otherwise({
                        redirectTo: '/'
                    });

            // use the HTML5 History API
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        }]);
})();