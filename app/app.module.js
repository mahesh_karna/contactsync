'use strict';
var glbApp;
(function () {
    glbApp = angular.module('glbApp', ['ngRoute', 'smart-table', 'ui.bootstrap', 'ngMaterial', 'mdPickers', 'angular-growl', 'ngCookies','angular-confirm','ngWebSocket']);
    glbApp.constant("siteURL", {
        "url": "http://dev.angpro1.com/",
    });
    glbApp.constant("assetURL", {
        "url": "http://dev.angpro1.com/app/assets/",
    });
    glbApp.constant("serviceURL", {
        "getAllUrl": "http://192.168.6.83:8080/RestWithSpring/jersey/hello/getAllContacts",
        "ENDPOINT": "http://192.168.6.83:8080/RestWithSpring/jersey/hello/contact",
        "UPDATE_ENDPOINT": "http://192.168.6.83:8080/RestWithSpring/jersey/hello/async"
    });
    glbApp.constant("updateRecursive", {
        "duration":10000
    });
})();