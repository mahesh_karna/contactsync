'use strict';

(function () {
    glbApp.directive('logoHeader', ['$location', function ($location) {
            return {
                templateUrl: 'app/shared/header/logoheader.html',
                restrict: 'E',
                replace: true,
            }
        }]);
})();