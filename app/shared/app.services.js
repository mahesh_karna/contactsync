'use strict';
(function () {

    glbApp.service('contactService', ['$http', '$rootScope', 'serviceURL', function ($http, $rootScope, serviceURL) {
            this.getcontacts = function (fn) {
                $http({
                    method: 'GET',
                    url: serviceURL.getAllUrl
                }).then(function successCallback(response) {
                    fn(response.data);
                    return;
                }, function errorCallback(response) {
                });
            }
            this.getLatestContacts = function (fn) {
                $http({
                    method: 'GET',
                    url: serviceURL.UPDATE_ENDPOINT,
                }).then(function successCallback(response) {
                    fn(response.data);
                    return;
                }, function errorCallback(response) {
                });
            }
            this.putcontacts = function (cnData, fn) {
                var data = cnData;
                var method = (typeof data.id == "undefined") ? 'PUT' : 'POST';
                $http({
                    method: method,
                    url: serviceURL.ENDPOINT,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function successCallback(response) {
                    fn(response.data);
                    return;
                }, function errorCallback(response) {
                });
            }
            this.removeContact = function (cid, fn) {
                $http({
                    method: 'DELETE',
                    url: serviceURL.ENDPOINT + "/" + cid,
                }).then(function successCallback(response) {
                    fn(response.data);
                    return;
                }, function errorCallback(response) {
                });
            }
        }]);
})();
